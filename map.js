
var degree,done,defualtDegree;
//console.log($("#topdiv>div").attr("name"));
function changePos(thisdiv){
	thisdiv.attr("class","dot active");
	$("#arrow").css({
		transform: 'rotate('+degree+'deg)',
	});
	$("#arrow").animate({
		top:thisdiv.css("top"),
		left:thisdiv.css("left"),
	},1500,"swing");
}
function receiveYaw(yaw){
	if(typeof degree=="undefined"){
		degree=0;
	}
	var angle=yaw+degree;
	$("#arrow").css({
		transform: 'rotate('+angle+'deg)'
	});
}
function calculateXY(nowdiv,prediv){
	var activeY=prediv.css("top").substr(0,prediv.css("top").length-2);
	var activeX=prediv.css("left").substr(0,prediv.css("left").length-2);
	var thisY=nowdiv.css("top").substr(0,nowdiv.css("top").length-2);
	var thisX=nowdiv.css("left").substr(0,nowdiv.css("left").length-2);
	var y=activeY-thisY;
	var x=activeX-thisX;
	degree=convertPositionAngel(y,x);
}

function convertPositionAngel(y,x)
{
  var res=(Math.atan2(y,x)) / Math.PI * 180.0;
  return(parseInt(res));
}
var nowDiv, preDiv;
function sceneIdToDiv(nowSceneId,preSceneId){
	$("#topdiv>div").each(function(){
		if($(this).attr("name")==nowSceneId){
			nowDiv=$(this);
		}
		if($(this).attr("name")==preSceneId){
			preDiv=$(this);
		}
	});
	changeDotStauts();
}

function changeDotStauts(){
	calculateXY(nowDiv,preDiv);
	$(".active").attr("class","dot nonactive");
	changePos(nowDiv);
}
var degree,done,defualtDegree;
//console.log($("#topdiv>div").attr("name"));
function changePos(thisdiv){
	thisdiv.attr("class","dot active");
	$("#arrow").css({
		transform: 'rotate('+degree+'deg)',
	});
	$("#arrow").animate({
		 opacity:0,
	},500,"swing",function(){
		$("#arrow").css({
			opacity:1,
			top:thisdiv.css("top"),
			left:thisdiv.css("left"),
		});
	});
}
function receiveYaw(yaw){
	if(typeof degree=="undefined"){
		degree=0;
	}
	var angle=yaw+degree;
	$("#arrow").css({
		transform: 'rotate('+angle+'deg)'
	});
}
function calculateXY(nowdiv,prediv){
	var activeY=prediv.css("top").substr(0,prediv.css("top").length-2);
	var activeX=prediv.css("left").substr(0,prediv.css("left").length-2);
	var thisY=nowdiv.css("top").substr(0,nowdiv.css("top").length-2);
	var thisX=nowdiv.css("left").substr(0,nowdiv.css("left").length-2);
	var y=activeY-thisY;
	var x=activeX-thisX;
	degree=convertPositionAngel(y,x);
}

function convertPositionAngel(y,x)
{
  var res=(Math.atan2(y,x)) / Math.PI * 180.0;
  return(parseInt(res));
}
var nowDiv, preDiv;
function sceneIdToDiv(nowSceneId,preSceneId){
	$("#topdiv>div").each(function(){
		if($(this).attr("name")==nowSceneId){
			nowDiv=$(this);
		}
		if($(this).attr("name")==preSceneId){
			preDiv=$(this);
		}
	});
	changeDotStauts();
}

function changeDotStauts(){
	calculateXY(nowDiv,preDiv);
	$(".active").attr("class","dot nonactive");
	changePos(nowDiv);
}

function markupThePath(path){
	for(var i in path){
		$("#topdiv>div").each(function(){
			if($(this).attr("name")==path[i]){
				$(this).attr("class","dot pathpoint");
			}
		});
	}

}